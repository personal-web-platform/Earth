/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { SyntheticEvent, useCallback, useRef } from 'react';
import { useSelector } from 'react-redux';

import Modal from '@components/modal/Modal';
import { useApiCall } from '@hooks/useApiCall';
import useInputField from '@hooks/useInputField';
import { AddMTInterface, storeInterface } from '@interfaces';
import { validURL } from '@utils/utils';

import styles from './Overlays.module.css';

function AddMT({ handleClose, open, handleFetchMasteredTechnologies }: AddMTInterface) {
    const user = useSelector((state: storeInterface) => state.user);

    const newTechnologyNameRef = useRef<HTMLInputElement>(null);
    const newTechnologyWebsiteUrlRef = useRef<HTMLInputElement>(null);
    const newTechnologyIconUrlRef = useRef<HTMLInputElement>(null);
    const newTechnologyCategoryRef = useRef<HTMLInputElement>(null);

    const { sendRequest: insertTechnology } = useApiCall();

    const {
        userValue: newTechnologyName,
        handleValueChange: newTechnologyNameOnChange,
        handleValueReset: newTechnologyNameOnReset,
        buildInputFieldCssClasses: buildTechnologyNameCssClass
    } = useInputField((value: string) => value.trim().length >= 2);

    const {
        userValue: newTechnologyWebsiteUrl,
        handleValueChange: newTechnologyWebsiteUrlOnChange,
        handleValueReset: newTechnologyWebsiteUrlOnReset,
        buildInputFieldCssClasses: buildTechnologyUrlCssClass
    } = useInputField(validURL);

    const {
        userValue: newTechnologyIconUrl,
        handleValueChange: newTechnologyIconUrlOnChange,
        handleValueReset: newTechnologyIconUrlOnReset,
        buildInputFieldCssClasses: buildTechnologyUrlIconCssClass
    } = useInputField(validURL);

    const {
        userValue: newTechnologyCategory,
        handleValueChange: newTechnologyCategoryOnChange,
        handleValueReset: newTechnologyCategoryOnReset,
        buildInputFieldCssClasses: buildTechnologyCategoryCssClass
    } = useInputField((value: string) => value.trim().length >= 2);

    const handleInsertNewLearningTechnology = useCallback(async (e: SyntheticEvent) => {
        e.preventDefault();
        if (newTechnologyName.valid && newTechnologyWebsiteUrl.valid && newTechnologyIconUrl.valid && newTechnologyCategory.valid) {
            try {
                await insertTechnology({
                    url: '/technologies',
                    method: 'POST',
                    authRequest: false,
                    body: {
                        'name': newTechnologyName.value,
                        'icon': newTechnologyIconUrl.value,
                        'link': newTechnologyWebsiteUrl.value,
                        'category': newTechnologyCategory.value,
                        'status': 'mastered'
                    },
                    headers: { token: user.token }
                });
                handleFetchMasteredTechnologies(false);
                handleClose();
            } catch (error) {
                alert(error);
            }
            newTechnologyNameOnReset();
            newTechnologyWebsiteUrlOnReset();
            newTechnologyIconUrlOnReset();
        }
    }, [newTechnologyName, newTechnologyWebsiteUrl, newTechnologyIconUrl, newTechnologyCategory, insertTechnology, handleFetchMasteredTechnologies, handleClose]);

    return <Modal handleClose={handleClose} isOpen={open}>
        <form onSubmit={handleInsertNewLearningTechnology} id={styles.form}>
            <input
                type={'text'}
                className={buildTechnologyNameCssClass(newTechnologyName)}
                ref={newTechnologyNameRef}
                onChange={newTechnologyNameOnChange}
                onClick={newTechnologyNameOnReset}
                value={newTechnologyName.value}
                placeholder={'Name'}
                autoFocus={true}
            />
            <input
                type={'text'}
                className={buildTechnologyUrlCssClass(newTechnologyWebsiteUrl)}
                ref={newTechnologyWebsiteUrlRef}
                onChange={newTechnologyWebsiteUrlOnChange}
                onClick={newTechnologyWebsiteUrlOnReset}
                value={newTechnologyWebsiteUrl.value}
                placeholder={'URL'}
            />
            <input
                type={'text'}
                className={buildTechnologyUrlIconCssClass(newTechnologyIconUrl)}
                ref={newTechnologyIconUrlRef}
                onChange={newTechnologyIconUrlOnChange}
                onClick={newTechnologyIconUrlOnReset}
                value={newTechnologyIconUrl.value}
                placeholder={'Logo URL'}
            />
            <input
                type={'text'}
                className={buildTechnologyCategoryCssClass(newTechnologyCategory)}
                ref={newTechnologyCategoryRef}
                onChange={newTechnologyCategoryOnChange}
                onClick={newTechnologyCategoryOnReset}
                value={newTechnologyCategory.value}
                placeholder={'Technology category'}
            />
            <button id={styles.sendButton}>Send</button>
        </form>
    </Modal>;
}

export default AddMT;