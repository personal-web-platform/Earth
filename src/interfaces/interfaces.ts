/*
 *
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import { MouseEventHandler } from 'react';

// Commons
export interface log {
    id: number,
    project: string,
    level: string,
    message: string,
    endpoint: string,
    requestMethod: string,
    requestIp: string,
    requestHostname: string,
    createdAt: string
}

export interface project {
    group_name: string,
    name: string,
    description: string,
    visibility: string,
    latest_tag: string,
    gitlab: string,
    created: string,
    last_activity: string
}

export interface technology {
    category?: string,
    name: string,
    icon: string,
    link: string,
    status: 'learning' | 'mastered'
}

// Components interfaces
export interface footerIconProps {
    url: string,
    logo: string
}

export interface modalProps {
    children: JSX.Element | JSX.Element[],
    isOpen: boolean,
    handleClose: MouseEventHandler<HTMLButtonElement>,
    cardTitle?: string
}

export interface overlayProps {
    children: JSX.Element | JSX.Element[],
    wrapperId: string
}

export interface tableProps {
    data: Array<any>,
    sortByName: boolean,
    children?: JSX.Element | JSX.Element[]
}

export interface cardProps {
    title?: string,
    children: JSX.Element | JSX.Element[],
    controls?: JSX.Element | JSX.Element[]
}

// Hooks interfaces
export interface inputField {
    edited: boolean,
    valid: boolean,
    value: string
}

// Section interfaces
export interface dashboardLogState {
    rawResponseData: rawLogsData,
    processedLogs: Array<processedLogs>,
    totalNumberOfLogs: number,
    numberOfPages: number
}

export interface processedStatistic {
    data: Array<number>,
    backgroundColor: Array<string>,
    borderColor: Array<string>
}

export interface processedStatisticsForGraphs {
    labels: Array<string>,
    datasets: Array<processedStatistic>
}

export interface statisticsState {
    rawStatistics: rawStatisticsData,
    processedLogRepartition: processedStatisticsForGraphs,
    processAverageLogsPerProject: processedStatisticsForGraphs,
    processAverageLogsPerDay: processedStatisticsForGraphs
}

export interface LearningTechnologiesStateInterface {
    rawLearningTechnologies: Array<technology>,
    processedLearningTechnologies: Array<processedTechnologies>
}

export interface MasteredTechnologiesStateInterface {
    rawMasteredTechnologies: Array<technology>,
    processedMasteredTechnologies: Array<processedTechnologies>,
}

export interface jsonParsedExportedTechnologies {
    technologies: Array<technology>,
    category: string
}

export interface AddLTInterface {
    handleClose: () => any,
    open: boolean,
    handleFetchLearningTechnologies: (useCache: boolean) => any
}

export interface AddMTInterface {
    handleClose: () => any,
    open: boolean,
    handleFetchMasteredTechnologies: (useCache: boolean) => any
}

export interface DeleteMTInterface {
    handleClose: () => any,
    open: boolean,
    handleFetchMasteredTechnologies: (useCache: boolean) => any,
    options: Array<string>
}

export interface DeleteLTInterface {
    handleClose: () => any,
    open: boolean,
    handleFetchLearningTechnologies: (useCache: boolean) => any,
    options: Array<string>
}

// Store interfaces
export interface storeInterface {
    user: userSliceInterface
}

export interface userSliceInterface {
    mail: string | null,
    token: string | null,
    exp: string | null,
    iat: string | null,
    role: Array<string> | null,
    isLoggedIn: boolean
}

export interface handleLoginRequestParameters {
    mail: string,
    password: string
}

// Utils interfaces
export interface useApiCallInterface {
    url: string
    method: 'GET' | 'POST' | 'DELETE',
    headers?: Object,
    dataTransformer?: Function,
    authRequest: Boolean,
    body?: Object,
    responseType?: string
}

export interface rawLogsData {
    totalNumberOfLogs: number,
    logs: Array<log>
}

export interface rawStatisticsData {
    totalNumberOfLogs: number,
    totalNumberOfProjects: number,
    totalNumberOfDays: number,
    numberOfLogsPerProject: Array<numberOfLogsPerProject>,
    numberOfLogsPerDay: Array<numberOfLogsPerDay>,
    averageNumberOfLogsPerProject: Array<averageNumberOfLogsPerProject>
}

export interface numberOfLogsPerProject {
    projectName: string,
    total: number,
}

export interface numberOfLogsPerDay {
    date: string,
    total: number
}

export interface averageNumberOfLogsPerProject {
    projectName: string,
    average: number,
}

export interface processedProject {
    text: string
}

export interface processedProjects {
    title: string,
    items: Array<processedProject>
}

export interface processedTechnology {
    text: string,
    icon: string,
    link: string
}

export interface processedTechnologies {
    title: string | undefined,
    items: Array<processedTechnology>
}

export interface processedLogsItems {
    text: string
}

export interface processedLogs {
    title: string,
    items: Array<processedLogsItems>
}