/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import '@testing-library/jest-dom';
import Card from '@components/card/Card';
import { render, screen } from '@testing-library/react';

describe('Card component test suite', () => {
    const props = [{
        title: 'card title'
    }];

    test('Should render the right node sequence', () => {
        // Run
        render(<Card title={props[0].title}>
            <button>button1</button>
            <button>button2</button>
        </Card>);
        const text = screen.getByText(props[0].title);
        const buttons = screen.getAllByRole('button');

        // Expect
        expect(text).toBeInTheDocument();
        expect(buttons.length).toEqual(2);
    });

    test('Should render controls if any', () => {
        // Setup
        const controlsMock = <button>Action1</button>;

        // Run
        render(<Card title='title' controls={controlsMock}>
            <p>This is a text</p>
        </Card>);
        const text = screen.getByText('Action1');
        const button = screen.getByRole('button');

        // Expect
        expect(text).toBeInTheDocument();
        expect(button).toBeInTheDocument();
    });
});