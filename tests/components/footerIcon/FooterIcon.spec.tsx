/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import '@testing-library/jest-dom';
import FooterIcon from '@components/footerIcon/FooterIcon';
import {render, screen} from '@testing-library/react';

describe('FooterIcon component test suite', () => {
    const props = [{
        logo: '',
        url: 'http://random.com/'
    }];

    test('Should render an a tag linking to website', () => {
        // Run
        render(<FooterIcon logo={props[0].logo} url={props[0].url}/>);
        const link = screen.getByRole('link');

        // Expect
        expect(link).toHaveAttribute('href', props[0].url);
        expect(link).toHaveAttribute('target', '_blank');
    });

    test('Should render an image', () => {
        // Run
        render(<FooterIcon logo={props[0].logo} url={props[0].url}/>);
        const img = screen.getByRole('presentation');

        // Expect
        expect(img).toBeInTheDocument();
        expect(img).toHaveAttribute('src', props[0].logo);
    });
});
