/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { ReactNode, ReactPortal } from 'react';
import ReactDOM from 'react-dom';

import '@testing-library/jest-dom';
import Modal from '@components/modal/Modal';
import { render, screen } from '@testing-library/react';

describe('Modal component test suite', () => {
    const props = [
        {
            isOpen: true,
            handleClose: () => {},
            cardTitle: 'title',
            children: 'text'
        },
        {
            isOpen: false,
            handleClose: () => {},
            cardTitle: 'title',
            children: 'text'
        }
    ];

    beforeAll(() => {
        ReactDOM.createPortal = (node: ReactNode): ReactPortal =>
            node as ReactPortal;
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    test('Should render a modal with the children inside', () => {
        // Run
        render(
            <Modal isOpen={props[0].isOpen} handleClose={props[0].handleClose}>
                <p>{props[0].children}</p>
            </Modal>
        );
        const text = screen.getByText(props[0].children);

        // Expect
        expect(text).toBeInTheDocument();
    });

    test('Should not render a modal if isOpen is false', () => {
        // Run
        render(
            <Modal isOpen={props[1].isOpen} handleClose={props[1].handleClose}>
                <p>{props[1].children}</p>
            </Modal>
        );
        const text = screen.queryByText(props[1].children);

        // Expect
        expect(text).toBeNull();
    });
});