/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { technology } from '../../../src/interfaces/interfaces';

export const rawLearningTechnologies: technology[] = [
    {
        'name': 'Kotlin',
        'icon': 'https://seeklogo.com/images/K/kotlin-logo-30C1970B05-seeklogo.com.png',
        'link': 'https://kotlinlang.org',
        'category': undefined,
        'status': 'learning'
    },
    {
        'name': 'C',
        'icon': 'https://img.favpng.com/10/23/21/c-programming-language-icon-png-favpng-878WK0RF2zxn7b6TimT7zquZN.jpg',
        'link': 'https://en.wikipedia.org/wiki/C_(programming_language)',
        'category': undefined,
        'status': 'learning'
    },
    {
        'name': 'Docker',
        'icon': 'https://docker.com',
        'link': 'https://docker.com',
        'category': undefined,
        'status': 'learning'
    }
];
export const rawMasteredTechnologies: technology[] = [
    {
        'name': 'Javascript',
        'icon': 'https://seeklogo.com/images/J/javascript-js-logo-2949701702-seeklogo.com.png',
        'link': 'https://www.javascript.com',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'Angular',
        'icon': 'https://seeklogo.com/images/A/angular-logo-B76B1CDE98-seeklogo.com.png',
        'link': 'https://angular.io',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'NodeJS',
        'icon': 'https://seeklogo.com/images/N/nodejs-logo-54107C5EDD-seeklogo.com.png',
        'link': 'https://nodejs.org/en/',
        'category': 'backend',
        'status': 'mastered'
    },
    {
        'name': 'Python',
        'icon': 'https://seeklogo.com/images/P/python-logo-A32636CAA3-seeklogo.com.png',
        'link': 'https://www.python.org',
        'category': 'backend',
        'status': 'mastered'
    },
    {
        'name': 'MariaDB',
        'icon': 'https://www.softizy.com/blog/wp-content/uploads/2014/05/mariadb.png',
        'link': 'https://mariadb.com',
        'category': 'database',
        'status': 'mastered'
    },
    {
        'name': 'MongoDB',
        'icon': 'https://res.cloudinary.com/practicaldev/image/fetch/s--a67KYY-A--/c_fill,f_auto,fl_progressive,h_320,q_auto,w_320/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/56177/3a0504e3-1139-4110-b903-08949636010a.jpg',
        'link': 'https://www.mongodb.com',
        'category': 'database',
        'status': 'mastered'
    },
    {
        'name': 'Gitlab',
        'icon': 'https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png',
        'link': 'https://about.gitlab.com',
        'category': 'deployment',
        'status': 'mastered'
    },
    {
        'name': 'HTML',
        'icon': 'https://seeklogo.com/images/H/html5-without-wordmark-color-logo-14D252D878-seeklogo.com.png',
        'link': 'https://whatwg.org',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'CSS',
        'icon': 'https://seeklogo.com/images/C/css3-logo-8724075274-seeklogo.com.png',
        'link': 'https://www.w3.org/Style/CSS/',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'ReactJS',
        'icon': 'https://seeklogo.com/images/R/react-logo-7B3CE81517-seeklogo.com.png',
        'link': 'https://reactjs.org',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'Docker',
        'icon': 'https://docker.com',
        'link': 'https://docker.com',
        'category': 'Deployment',
        'status': 'mastered'
    }
];