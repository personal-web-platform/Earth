/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import '@testing-library/jest-dom';
import Table from '@components/table/Table';
import { render, screen } from '@testing-library/react';

import { processRawLearningTechnologies, processRawMasteredTechnologies } from '../../../src/utils/utils';

import { rawLearningTechnologies, rawMasteredTechnologies } from './data';

describe('Table component test suite', () => {
    const props = [
        {
            data: processRawMasteredTechnologies(rawMasteredTechnologies),
            sortByName: true
        },
        {
            data: processRawLearningTechnologies(rawLearningTechnologies),
            sortByName: true
        }
    ];

    test('Should display a table', () => {
        // Run
        render(<Table data={props[1].data} sortByName={props[1].sortByName}/>);
        const tableElement = screen.getByRole('table');

        // Expect
        expect(tableElement).toBeInTheDocument();
    });

    test('Should render 3 images and 3 links', () => {
        // Run
        render(<Table data={props[1].data} sortByName={props[1].sortByName}/>);
        const images = screen.getAllByRole('img');
        const links = screen.getAllByRole('link');

        // Expect
        expect(images.length).toEqual(3);
        expect(links.length).toEqual(3);
    });

    test('Should render 4 rows', () => {
        // Run
        render(<Table data={props[1].data} sortByName={props[1].sortByName}/>);
        const rows = screen.getAllByRole('tableRow');

        // Expect
        expect(rows.length).toEqual(4);
    });

    test('Should render 11 images and 11 links', () => {
        // Run
        render(<Table data={props[0].data} sortByName={props[0].sortByName}/>);
        const images = screen.getAllByRole('img');
        const links = screen.getAllByRole('link');

        // Expect
        expect(images.length).toEqual(11);
        expect(links.length).toEqual(11);
    });

    test('Should render 6 rows', () => {
        // Run
        render(<Table data={props[0].data} sortByName={props[0].sortByName}/>);
        const rows = screen.getAllByRole('tableRow');

        // Expect
        expect(rows.length).toEqual(6);
    });
});