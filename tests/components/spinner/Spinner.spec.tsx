/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { act } from 'react';

import '@testing-library/jest-dom';
import Spinner from '@components/spinner/Spinner';
import { render, screen } from '@testing-library/react';

describe('Spinner component test suite', () => {
    jest.useFakeTimers();

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('Should not have a text before the timeout', () => {
        // Run
        render(<Spinner/>);
        const text = screen.queryByText('An error occured while fetching the data');

        // Expect
        expect(text).toBeNull();
    });

    test('Should render a text after the timeout', () => {
        // Setup
        jest.spyOn(global, 'setTimeout');

        // Run
        render(<Spinner/>);
        act(() => {
            jest.advanceTimersByTime(10000);
        });

        // Expect
        expect(setTimeout).toHaveBeenCalledTimes(2);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 10000);
        const text = screen.getByText('An error occured while fetching the data');
        expect(text).toBeInTheDocument();
    });
});