/*
 *
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


import { technology } from '../src/interfaces/interfaces';
import { convertIsoStringDateToDate } from '../src/utils/utils';

export const urls = ['https://www.francoisdevilez.eu', 'someUrl'];
export const roles = ['user', 'admin'];
export const rawProjectData = [
    {
        'groupName': 'Personal Web Platform',
        'name': 'Earth',
        'description': 'Earth is the frontend part of my personal web platform. Written in ReactJS.',
        'createdAt': '2020-07-23T00:00:00Z',
        'lastActivityAt': '2023-09-07T00:00:00Z',
        'webUrl': 'https://gitlab.com/personal-web-platform/Earth',
        'visibility': 'PUBLIC',
        'latestTag': '1.42.0',
        'language': 'TSX'
    }
];
export const expectedProjectData = [
    {
        title: 'Group name',
        items: [
            {
                text: 'Personal Web Platform'
            }
        ]
    },
    {
        title: 'Name',
        items: [
            {
                text: 'Earth'
            }
        ]
    },
    {
        title: 'Description',
        items: [
            {
                text: 'Earth is the frontend part of my personal web platform. Written in ReactJS.'
            }
        ]
    },
    {
        title: 'Created at',
        items: [
            {
                text: convertIsoStringDateToDate('2020-07-23T00:00:00Z')
            }
        ]
    },
    {
        title: 'Last activity at',
        items: [
            {
                text: convertIsoStringDateToDate('2023-09-07T00:00:00Z')
            }
        ]
    },
    {
        title: 'Visibility',
        items: [
            {
                text: 'PUBLIC'
            }
        ]
    },
    {
        title: 'Latest tag',
        items: [
            {
                text: '1.42.0'
            }
        ]
    },
    {
        title: 'Language',
        items: [
            {
                text: 'TSX'
            }
        ]
    },
    {
        title: 'Web url',
        items: [
            {
                text: 'https://gitlab.com/personal-web-platform/Earth'
            }
        ]
    }
];
export const rawLearningTechnologies: technology[] = [
    {
        'name': 'Kotlin',
        'icon': 'https://seeklogo.com/images/K/kotlin-logo-30C1970B05-seeklogo.com.png',
        'link': 'https://kotlinlang.org',
        'status': 'learning'
    },
    {
        'name': 'C',
        'icon': 'https://img.favpng.com/10/23/21/c-programming-language-icon-png-favpng-878WK0RF2zxn7b6TimT7zquZN.jpg',
        'link': 'https://en.wikipedia.org/wiki/C_(programming_language)',
        'status': 'learning'
    },
    {
        'name': 'Docker',
        'icon': 'https://docker.com',
        'link': 'https://docker.com',
        'status': 'learning'
    }
];
export const expectedProcessedLearningTechnologies = [
    {
        'items': [
            {
                'text': 'Kotlin',
                'icon': 'https://seeklogo.com/images/K/kotlin-logo-30C1970B05-seeklogo.com.png',
                'link': 'https://kotlinlang.org'
            },
            {
                'text': 'C',
                'icon': 'https://img.favpng.com/10/23/21/c-programming-language-icon-png-favpng-878WK0RF2zxn7b6TimT7zquZN.jpg',
                'link': 'https://en.wikipedia.org/wiki/C_(programming_language)'
            },
            {
                'text': 'Docker',
                'icon': 'https://docker.com',
                'link': 'https://docker.com'
            }
        ]
    }
];
export const rawMasteredTechnologies: technology[] = [
    {
        'name': 'Javascript',
        'icon': 'https://seeklogo.com/images/J/javascript-js-logo-2949701702-seeklogo.com.png',
        'link': 'https://www.javascript.com',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'Angular',
        'icon': 'https://seeklogo.com/images/A/angular-logo-B76B1CDE98-seeklogo.com.png',
        'link': 'https://angular.io',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'NodeJS',
        'icon': 'https://seeklogo.com/images/N/nodejs-logo-54107C5EDD-seeklogo.com.png',
        'link': 'https://nodejs.org/en/',
        'category': 'backend',
        'status': 'mastered'
    },
    {
        'name': 'Python',
        'icon': 'https://seeklogo.com/images/P/python-logo-A32636CAA3-seeklogo.com.png',
        'link': 'https://www.python.org',
        'category': 'backend',
        'status': 'mastered'
    },
    {
        'name': 'MariaDB',
        'icon': 'https://www.softizy.com/blog/wp-content/uploads/2014/05/mariadb.png',
        'link': 'https://mariadb.com',
        'category': 'database',
        'status': 'mastered'
    },
    {
        'name': 'MongoDB',
        'icon': 'https://res.cloudinary.com/practicaldev/image/fetch/s--a67KYY-A--/c_fill,f_auto,fl_progressive,h_320,q_auto,w_320/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/56177/3a0504e3-1139-4110-b903-08949636010a.jpg',
        'link': 'https://www.mongodb.com',
        'category': 'database',
        'status': 'mastered'
    },
    {
        'name': 'Gitlab',
        'icon': 'https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png',
        'link': 'https://about.gitlab.com',
        'category': 'deployment',
        'status': 'mastered'
    },
    {
        'name': 'HTML',
        'icon': 'https://seeklogo.com/images/H/html5-without-wordmark-color-logo-14D252D878-seeklogo.com.png',
        'link': 'https://whatwg.org',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'CSS',
        'icon': 'https://seeklogo.com/images/C/css3-logo-8724075274-seeklogo.com.png',
        'link': 'https://www.w3.org/Style/CSS/',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'ReactJS',
        'icon': 'https://seeklogo.com/images/R/react-logo-7B3CE81517-seeklogo.com.png',
        'link': 'https://reactjs.org',
        'category': 'frontend',
        'status': 'mastered'
    },
    {
        'name': 'Docker',
        'icon': 'https://docker.com',
        'link': 'https://docker.com',
        'category': 'Deployment',
        'status': 'mastered'
    }
];
export const expectedProcessedMasteredTechnologies = [
    {
        'items': [
            {
                'icon': 'https://docker.com',
                'link': 'https://docker.com',
                'text': 'Docker'
            }
        ],
        'title': 'Deployment'
    },
    {
        'items': [
            {
                'icon': 'https://seeklogo.com/images/N/nodejs-logo-54107C5EDD-seeklogo.com.png',
                'link': 'https://nodejs.org/en/',
                'text': 'NodeJS'
            },
            {
                'icon': 'https://seeklogo.com/images/P/python-logo-A32636CAA3-seeklogo.com.png',
                'link': 'https://www.python.org',
                'text': 'Python'
            }
        ],
        'title': 'backend'
    },
    {
        'items': [
            {
                'icon': 'https://www.softizy.com/blog/wp-content/uploads/2014/05/mariadb.png',
                'link': 'https://mariadb.com',
                'text': 'MariaDB'
            },
            {
                'icon': 'https://res.cloudinary.com/practicaldev/image/fetch/s--a67KYY-A--/c_fill,f_auto,fl_progressive,h_320,q_auto,w_320/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/56177/3a0504e3-1139-4110-b903-08949636010a.jpg',
                'link': 'https://www.mongodb.com',
                'text': 'MongoDB'
            }
        ],
        'title': 'database'
    },
    {
        'items': [
            {
                'icon': 'https://seeklogo.com/images/G/gitlab-logo-757620E430-seeklogo.com.png',
                'link': 'https://about.gitlab.com',
                'text': 'Gitlab'
            }
        ],
        'title': 'deployment'
    },
    {
        'items': [
            {
                'icon': 'https://seeklogo.com/images/J/javascript-js-logo-2949701702-seeklogo.com.png',
                'link': 'https://www.javascript.com',
                'text': 'Javascript'
            },
            {
                'icon': 'https://seeklogo.com/images/A/angular-logo-B76B1CDE98-seeklogo.com.png',
                'link': 'https://angular.io',
                'text': 'Angular'
            },
            {
                'icon': 'https://seeklogo.com/images/H/html5-without-wordmark-color-logo-14D252D878-seeklogo.com.png',
                'link': 'https://whatwg.org',
                'text': 'HTML'
            },
            {
                'icon': 'https://seeklogo.com/images/C/css3-logo-8724075274-seeklogo.com.png',
                'link': 'https://www.w3.org/Style/CSS/',
                'text': 'CSS'
            },
            {
                'icon': 'https://seeklogo.com/images/R/react-logo-7B3CE81517-seeklogo.com.png',
                'link': 'https://reactjs.org',
                'text': 'ReactJS'
            }
        ],
        'title': 'frontend'
    }
];

export const pageSize = 50;
export const rawLogs = {
    'totalNumberOfLogs': 28448,
    'logs': [
        {
            'id': 28446,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:50 GMT'
        },
        {
            'id': 28445,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:50 GMT'
        },
        {
            'id': 28444,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:09:50 GMT'
        },
        {
            'id': 28443,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:09:50 GMT'
        },
        {
            'id': 28442,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:32 GMT'
        },
        {
            'id': 28441,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching log statistics',
            'endpoint': '/logs/statistics',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:32 GMT'
        },
        {
            'id': 28440,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'Getting log statistics',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:32 GMT'
        },
        {
            'id': 28439,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:31 GMT'
        },
        {
            'id': 28438,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all logs',
            'endpoint': '/logs/list?pageNumber=0&pageSize=50',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:31 GMT'
        },
        {
            'id': 28437,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'Getting all logs - page number: 0 - page size: 50',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:31 GMT'
        },
        {
            'id': 28436,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28435,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28434,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28433,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28432,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28431,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account logged in: root117@test.com',
            'endpoint': '/account/login/',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:30 GMT'
        },
        {
            'id': 28428,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28429,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28430,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28427,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all mastered technologies',
            'endpoint': '/masteredtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28426,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all repositories',
            'endpoint': '/repositories/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28425,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all learning technologies',
            'endpoint': '/learningtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:09:22 GMT'
        },
        {
            'id': 28424,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:11 GMT'
        },
        {
            'id': 28423,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching log statistics',
            'endpoint': '/logs/statistics',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:07:11 GMT'
        },
        {
            'id': 28422,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'Getting log statistics',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:11 GMT'
        },
        {
            'id': 28421,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28420,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all logs',
            'endpoint': '/logs/list?pageNumber=0&pageSize=50',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28419,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'Getting all logs - page number: 0 - page size: 50',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28418,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28417,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28416,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28415,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account verified: root117@test.com',
            'endpoint': '/account/verify',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.3',
            'requestHostname': 'moon',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28414,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28413,
            'project': 'Moon',
            'level': 'INFO',
            'message': 'Account logged in: root117@test.com',
            'endpoint': '/account/login/',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:07:10 GMT'
        },
        {
            'id': 28411,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28412,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28410,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28409,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all mastered technologies',
            'endpoint': '/masteredtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28408,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all repositories',
            'endpoint': '/repositories/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28407,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all learning technologies',
            'endpoint': '/learningtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:06:22 GMT'
        },
        {
            'id': 28406,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Moon',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:05:57 GMT'
        },
        {
            'id': 28405,
            'project': 'Moon',
            'level': 'ERROR',
            'message': 'Incorrect credentials!',
            'endpoint': '/account/login/',
            'requestMethod': 'POST',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:05:57 GMT'
        },
        {
            'id': 28404,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28402,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28403,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Neptune',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28401,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all repositories',
            'endpoint': '/repositories/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28400,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all mastered technologies',
            'endpoint': '/masteredtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28399,
            'project': 'Neptune',
            'level': 'INFO',
            'message': 'Fetching all learning technologies',
            'endpoint': '/learningtechnologies/list',
            'requestMethod': 'GET',
            'requestIp': '::ffff:172.18.0.1',
            'requestHostname': 'localhost',
            'createdAt': 'Tue, 24 Jan 2023 20:05:21 GMT'
        },
        {
            'id': 28398,
            'project': 'Sun',
            'level': 'INFO',
            'message': 'New log inserted from: Mercury',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': '-',
            'requestHostname': '-',
            'createdAt': 'Tue, 24 Jan 2023 20:00:13 GMT'
        },
        {
            'id': 28397,
            'project': 'Mercury',
            'level': 'INFO',
            'message': 'Job[1] Finished',
            'endpoint': '-',
            'requestMethod': '-',
            'requestIp': 'undefined',
            'requestHostname': 'undefined',
            'createdAt': 'Tue, 24 Jan 2023 20:00:13 GMT'
        }
    ]
};
export const expectedProcessedLogs = [
    {
        'title': 'id',
        'items': [
            {
                'text': 28446
            },
            {
                'text': 28445
            },
            {
                'text': 28444
            },
            {
                'text': 28443
            },
            {
                'text': 28442
            },
            {
                'text': 28441
            },
            {
                'text': 28440
            },
            {
                'text': 28439
            },
            {
                'text': 28438
            },
            {
                'text': 28437
            },
            {
                'text': 28436
            },
            {
                'text': 28435
            },
            {
                'text': 28434
            },
            {
                'text': 28433
            },
            {
                'text': 28432
            },
            {
                'text': 28431
            },
            {
                'text': 28428
            },
            {
                'text': 28429
            },
            {
                'text': 28430
            },
            {
                'text': 28427
            },
            {
                'text': 28426
            },
            {
                'text': 28425
            },
            {
                'text': 28424
            },
            {
                'text': 28423
            },
            {
                'text': 28422
            },
            {
                'text': 28421
            },
            {
                'text': 28420
            },
            {
                'text': 28419
            },
            {
                'text': 28418
            },
            {
                'text': 28417
            },
            {
                'text': 28416
            },
            {
                'text': 28415
            },
            {
                'text': 28414
            },
            {
                'text': 28413
            },
            {
                'text': 28411
            },
            {
                'text': 28412
            },
            {
                'text': 28410
            },
            {
                'text': 28409
            },
            {
                'text': 28408
            },
            {
                'text': 28407
            },
            {
                'text': 28406
            },
            {
                'text': 28405
            },
            {
                'text': 28404
            },
            {
                'text': 28402
            },
            {
                'text': 28403
            },
            {
                'text': 28401
            },
            {
                'text': 28400
            },
            {
                'text': 28399
            },
            {
                'text': 28398
            },
            {
                'text': 28397
            }
        ]
    },
    {
        'title': 'project',
        'items': [
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Moon'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Neptune'
            },
            {
                'text': 'Sun'
            },
            {
                'text': 'Mercury'
            }
        ]
    },
    {
        'title': 'level',
        'items': [
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'ERROR'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            },
            {
                'text': 'INFO'
            }
        ]
    },
    {
        'title': 'message',
        'items': [
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching log statistics'
            },
            {
                'text': 'Getting log statistics'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching all logs'
            },
            {
                'text': 'Getting all logs - page number: 0 - page size: 50'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Account logged in: root117@test.com'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching all mastered technologies'
            },
            {
                'text': 'Fetching all repositories'
            },
            {
                'text': 'Fetching all learning technologies'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching log statistics'
            },
            {
                'text': 'Getting log statistics'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching all logs'
            },
            {
                'text': 'Getting all logs - page number: 0 - page size: 50'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'Account verified: root117@test.com'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Account logged in: root117@test.com'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching all mastered technologies'
            },
            {
                'text': 'Fetching all repositories'
            },
            {
                'text': 'Fetching all learning technologies'
            },
            {
                'text': 'New log inserted from: Moon'
            },
            {
                'text': 'Incorrect credentials!'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'New log inserted from: Neptune'
            },
            {
                'text': 'Fetching all repositories'
            },
            {
                'text': 'Fetching all mastered technologies'
            },
            {
                'text': 'Fetching all learning technologies'
            },
            {
                'text': 'New log inserted from: Mercury'
            },
            {
                'text': 'Job[1] Finished'
            }
        ]
    },
    {
        'title': 'endpoint',
        'items': [
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '-'
            },
            {
                'text': '/logs/statistics'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/logs/list?pageNumber=0&pageSize=50'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/login/'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/masteredtechnologies/list'
            },
            {
                'text': '/repositories/list'
            },
            {
                'text': '/learningtechnologies/list'
            },
            {
                'text': '-'
            },
            {
                'text': '/logs/statistics'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/logs/list?pageNumber=0&pageSize=50'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '/account/verify'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/login/'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/masteredtechnologies/list'
            },
            {
                'text': '/repositories/list'
            },
            {
                'text': '/learningtechnologies/list'
            },
            {
                'text': '-'
            },
            {
                'text': '/account/login/'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '/repositories/list'
            },
            {
                'text': '/masteredtechnologies/list'
            },
            {
                'text': '/learningtechnologies/list'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            }
        ]
    },
    {
        'title': 'requestMethod',
        'items': [
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': 'POST'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': 'GET'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            }
        ]
    },
    {
        'title': 'requestIp',
        'items': [
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '::ffff:172.18.0.3'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '::ffff:172.18.0.1'
            },
            {
                'text': '-'
            },
            {
                'text': 'undefined'
            }
        ]
    },
    {
        'title': 'requestHostname',
        'items': [
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'moon'
            },
            {
                'text': 'moon'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'moon'
            },
            {
                'text': 'moon'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'moon'
            },
            {
                'text': 'moon'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': '-'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': 'localhost'
            },
            {
                'text': '-'
            },
            {
                'text': 'undefined'
            }
        ]
    },
    {
        'title': 'createdAt',
        'items': [
            {
                'text': 'Tue, 24 Jan 2023 20:09:50 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:50 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:50 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:50 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:32 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:32 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:32 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:31 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:31 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:31 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:30 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:09:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:11 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:11 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:11 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:07:10 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:06:22 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:57 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:57 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:05:21 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:00:13 GMT'
            },
            {
                'text': 'Tue, 24 Jan 2023 20:00:13 GMT'
            }
        ]
    }
];

export const rawStatistics = {
    'success': true,
    'data': {
        'success': true,
        'data': {
            'totalNumberOfLogs': 29859,
            'totalNumberOfProjects': 9,
            'totalNumberOfDays': 47,
            'numberOfLogsPerProject': [
                {
                    'projectName': 'neptune',
                    'total': 4352
                },
                {
                    'projectName': 'moon',
                    'total': 1713
                },
                {
                    'projectName': 'mercury',
                    'total': 8485
                },
                {
                    'projectName': 'sun',
                    'total': 15306
                },
                {
                    'projectName': 'star',
                    'total': 0
                },
                {
                    'projectName': 'earth',
                    'total': 0
                },
                {
                    'projectName': 'database',
                    'total': 0
                },
                {
                    'projectName': 'database2',
                    'total': 0
                },
                {
                    'projectName': 'pwp-testware',
                    'total': 0
                }
            ],
            'numberOfLogsPerDay': [
                {
                    'date': '2023-02-22T00:00:00.000Z',
                    'total': 946
                }
            ],
            'averageNumberOfLogsPerProject': [
                {
                    'projectName': 'neptune',
                    'average': 290
                },
                {
                    'projectName': 'moon',
                    'average': 132
                },
                {
                    'projectName': 'mercury',
                    'average': 181
                },
                {
                    'projectName': 'sun',
                    'average': 326
                },
                {
                    'projectName': 'star',
                    'average': 0
                },
                {
                    'projectName': 'earth',
                    'average': 0
                },
                {
                    'projectName': 'database',
                    'average': 0
                },
                {
                    'projectName': 'database2',
                    'average': 0
                },
                {
                    'projectName': 'pwp-testware',
                    'average': 0
                }
            ]
        },
        'error': []
    },
    'error': []
};
export const expectedLogRepartitionData = {
    'labels': [
        'neptune',
        'moon',
        'mercury',
        'sun',
        'star',
        'earth',
        'database',
        'database2',
        'pwp-testware'
    ],
    'datasets': [
        {
            'data': [
                4352,
                1713,
                8485,
                15306,
                0,
                0,
                0,
                0,
                0
            ],
            'backgroundColor': [
                '#8aa3ff',
                '#b5b5b5',
                '#806f63',
                '#fdff8a',
                '#fff6f0',
                '#97ff8a',
                'pink',
                'orange',
                'lightblue'
            ],
            'borderColor': [
                '#8aa3ff',
                '#b5b5b5',
                '#806f63',
                '#fdff8a',
                '#fff6f0',
                '#97ff8a',
                'pink',
                'orange',
                'lightblue'
            ]
        }
    ]
};
export const expectedAverageLogPerProjectPerDay = {
    'labels': [
        'neptune',
        'moon',
        'mercury',
        'sun',
        'star',
        'earth',
        'database',
        'database2',
        'pwp-testware'
    ],
    'datasets': [
        {
            'data': [
                290,
                132,
                181,
                326,
                0,
                0,
                0,
                0,
                0
            ],
            'backgroundColor': [
                '#8aa3ff',
                '#b5b5b5',
                '#806f63',
                '#fdff8a',
                '#fff6f0',
                '#97ff8a',
                'pink',
                'orange',
                'lightblue'
            ],
            'borderColor': [
                '#8aa3ff',
                '#b5b5b5',
                '#806f63',
                '#fdff8a',
                '#fff6f0',
                '#97ff8a',
                'pink',
                'orange',
                'lightblue'
            ]
        }
    ]
};
export const expectedAverageLogPerDay = {
    'labels': [
        'Wed Feb 22 2023'
    ],
    'datasets': [
        {
            'data': [
                946
            ],
            'backgroundColor': [
                '#8aa3ff',
                '#b5b5b5',
                '#806f63',
                '#fdff8a',
                '#fff6f0',
                '#97ff8a',
                'pink',
                'orange',
                'lightblue'
            ],
            'borderColor': [
                '#729EA1'
            ]
        }
    ]
};