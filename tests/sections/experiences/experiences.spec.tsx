/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';

import '@testing-library/jest-dom';
import {jobTimelineData} from '@data';
import {dictionary} from '@dictionary';
import Experiences from '@sections/experiences/Experiences';
import {render, screen} from '@testing-library/react';
import '../../matchMedia';

describe('Experiences section test suite', () => {
    const intersectionObserverMock = () => ({
        observe: jest.fn(),
        disconnect: jest.fn()
    });

    window.IntersectionObserver = jest.fn().mockImplementation(intersectionObserverMock);
    let alertMock: jest.SpyInstance<void, [message?: any]>;

    beforeEach(() => {
        alertMock = jest.spyOn(window, 'alert').mockImplementation();
    });

    const jobTitles = jobTimelineData.map((job) => {
        return job.title;
    });

    test('Should render the title', () => {
        // Run
        render(<Experiences/>);
        const title = screen.getByText(dictionary.sections.experiences.title);

        // Expect
        expect(title).toBeInTheDocument();
        expect(title).toBeDefined();
    });

    jobTitles.forEach((jobTitle) => {
        test('Should render the job title - ' + jobTitle, () => {
            // Run
            render(<Experiences/>);
            const title = screen.getByText(jobTitle);

            // Expect
            expect(title).toBeInTheDocument();
            expect(title).toBeDefined();
        });
    });
});