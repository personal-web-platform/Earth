/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, { act } from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { dictionary } from '@dictionary';
import * as useApiCall from '@hooks/useApiCall';
import Projects from '@sections/projects/Projects';
import { render, screen } from '@testing-library/react';
import cache from '@utils/cache';

describe('Projects section test suite', () => {
    const mockStore = configureMockStore();
    const renderComponent = async (isLoggedIn: boolean) => {
        return render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: isLoggedIn,
                    role: [dictionary.roles.user, dictionary.roles.admin],
                    token: 'token'
                }
            })}>
                <Projects/>
            </Provider>
        );
    };

    const mockData = [
        {
            'group_name': 'group_name',
            'name': 'name',
            'description': 'description',
            'visibility': 'visibility',
            'latest_tag': '1.0.0',
            'gitlab': 'https://gitlab.com',
            'created': '2021-12-20T21:44:15.612Z',
            'last_activity': '2022-03-30T21:23:06.801Z'
        }
    ];

    let alertMock: jest.SpyInstance<void, [message?: any]>;

    beforeEach(() => {
        alertMock = jest.spyOn(window, 'alert').mockImplementation();
        jest.useFakeTimers();
    });

    afterEach(() => {
        jest.useRealTimers();
        jest.restoreAllMocks();
    });

    test('Should render the section title', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(false);
        });
        const title = screen.getByText(dictionary.sections.projects.title);

        // Expect
        expect(title).toBeDefined();
        expect(title).toBeInTheDocument();
    });

    test('Should show a loader while fetching projects', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: true,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        const { container } = await renderComponent(false);
        const loaders = container.getElementsByClassName('loader');
        expect(loaders.length).toEqual(1);
    });

    test('Should render the admin button if the user is logged in', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await renderComponent(true);
        const adminButtonTitle = screen.getByText(dictionary.sections.technologies.adminButton.commons.title);

        // Expect
        expect(adminButtonTitle).toBeInTheDocument();
        expect(adminButtonTitle).toBeDefined();
    });

    test('Should render an error if no data has been fetched', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: true,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(null)
        });

        // Run
        await renderComponent(false);
        act(() => {
            jest.runAllTimers();
        });
        const errorMessage = screen.getByText('An error occured while fetching the data');

        // Expect
        expect(errorMessage).toBeDefined();
        expect(errorMessage).toBeInTheDocument();
    });

    test('Should render a table with projects', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(false);
            jest.runAllTimers();
        });
        const table = screen.getByRole('table');
        const tableRow = screen.getAllByRole('tableRow');

        // Expect
        expect(table).toBeInTheDocument();
        expect(table).toBeDefined();
        expect(tableRow.length).toBe(2);
    });

    test('Should use the cache if the data has been cached', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });
        jest.spyOn(cache, 'get').mockReturnValue(mockData);

        // Run
        await act(async () => {
            await renderComponent(false);
            jest.runAllTimers();
        });
        const table = screen.getByRole('table');
        const tableRow = screen.getAllByRole('tableRow');

        // Expect
        expect(table).toBeInTheDocument();
        expect(table).toBeDefined();
        expect(tableRow.length).toBe(2);
    });

    test('Should alert an error if the api call failed', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: () => {
                throw new Error('');
            }
        });

        // Run
        await act(async () => {
            await renderComponent(false);
            jest.runAllTimers();
        });
        const title = screen.getByText('Personal projects');

        // Expect
        expect(title).toBeDefined();
        expect(title).toBeInTheDocument();
        expect(alertMock).toHaveBeenCalledTimes(1);
    });

    test('Should call an api call when exporting technologies', async () => {
        // Setup
        const mockData = {
            'success': true,
            'data': [
                {
                    'category': 'mastered',
                    'technologies': [
                        {
                            'name': 'MariaDB',
                            'icon': 'https://www.softizy.com/blog/wp-content/uploads/2014/05/mariadb.png',
                            'link': 'https://mariadb.com',
                            'category': 'database'
                        }
                    ]
                }
            ],
            'error': []
        };
        jest.spyOn(cache, 'get').mockReturnValue(null);
        Blob.prototype.text = jest.fn().mockResolvedValue(JSON.stringify(mockData.data));
        const exportFunction = jest.fn().mockReturnValue(mockData);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: exportFunction
        });
        window.URL.createObjectURL = jest.fn();

        // Run
        await act(async () => {
            await renderComponent(true);
            jest.runAllTimers();
        });
        const button = screen.getByText(dictionary.sections.technologies.adminButton.commons.exportTechnologies);
        button.click();

        // Expect
        expect(exportFunction).toHaveBeenCalledTimes(2);
    });
});