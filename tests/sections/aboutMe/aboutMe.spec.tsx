/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { act } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { dictionary } from '@dictionary';
import AboutMe from '@sections/aboutMe/AboutMe';
import * as userSlice from '@store/slices/userSlice';
import { render, screen } from '@testing-library/react';

const navigateSpy = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => navigateSpy
}));

describe('About me section test suite', () => {
    const mockStore = configureMockStore();

    const renderComponent = (isLoggedIn: boolean) => {
        const { container, debug } = render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: isLoggedIn
                }
            })}>
                <BrowserRouter>
                    <Routes>
                        <Route path={''} element={<AboutMe/>}/>
                    </Routes>
                </BrowserRouter>
            </Provider>
        );
        return { container, debug };
    };

    test('Should display the login button if user is logged in', () => {
        // Run
        renderComponent(true);
        const button = screen.getByText('Logout');

        // Expect
        expect(button).toBeInTheDocument();
    });

    test('Should display the login button if user is not logged in', () => {
        // Run
        renderComponent(false);
        const button = screen.getByText('Login');

        // Expect
        expect(button).toBeInTheDocument();
    });


    test('Should display the welcome texts', () => {
        // Run
        renderComponent(false);
        const welcomeTextOne = screen.getByText(dictionary.sections.aboutMe.welcomeText);
        const welcomeTextTwo = screen.getByText(dictionary.sections.aboutMe.welcomeText2);

        // Expect
        expect(welcomeTextOne).toBeDefined();
        expect(welcomeTextOne).toBeInTheDocument();
        expect(welcomeTextTwo).toBeDefined();
        expect(welcomeTextTwo).toBeInTheDocument();
    });

    test('Should display a link to download cv', () => {
        // Run
        renderComponent(false);
        const link = screen.getByRole('link');

        // Expect
        expect(link).toBeInTheDocument();
    });

    test('Should display the welcome picture', () => {
        // Run
        renderComponent(false);
        const img = screen.getByRole('img');

        // Expect
        expect(img).toBeInTheDocument();
    });

    test('Should go on the login page when clicking the login link', async () => {
        // Run
        renderComponent(false);
        const loginButton = screen.getByText('Login');
        await act(() => {
            loginButton.click();
        });

        // Expect
        expect(navigateSpy).toHaveBeenCalledTimes(1);
        expect(navigateSpy).toHaveBeenCalledWith('/login');
    });

    test('Should dispatch a logout event when clicking on logout', async () => {
        // Setup
        const handleLogoutSpy = jest.spyOn(userSlice, 'handleLogoutRequest').mockReturnValue({
            type: '',
            success: true
        } as never);

        // Run
        renderComponent(true);
        const logoutButton = screen.getByText('Logout');
        await act(() => {
            logoutButton.click();
        });

        // Expect
        expect(handleLogoutSpy).toHaveBeenCalledTimes(1);
    });
});