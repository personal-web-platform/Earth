/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import * as useApiCall from '../../../../../src/hooks/useApiCall';
import DeleteLT from '../../../../../src/sections/technologies/learning/overlays/DeleteLT';

describe('Delete learning technology overlay test suite', () => {
    const modalRoot = global.document.createElement('div');
    modalRoot.setAttribute('id', 'portal-root');
    const body = global.document.querySelector('body');
    body ? body.appendChild(modalRoot) : null;

    const handleCloseMock = jest.fn();
    const handleFetchLearningTechnologiesMock = jest.fn();

    const returnDataFunction = jest.fn().mockResolvedValue(null);

    const mockStore = configureMockStore();
    const renderComponent = async () => {
        return render(
            <Provider store={mockStore({ user: { token: 'token' } })}>
                <DeleteLT handleClose={handleCloseMock} open={true} handleFetchLearningTechnologies={handleFetchLearningTechnologiesMock} options={['1', '2']}/>
            </Provider>
        );
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('Should render the portal', async () => {
        // Run
        await renderComponent();
        screen.debug();
        const inputs = screen.getAllByRole('combobox');
        const closeButton = screen.getByRole('img');

        // Expect
        expect(inputs.length).toEqual(1);
        expect(closeButton).toBeInTheDocument();
        expect(closeButton).toBeDefined();
    });

    test('Should send an api request if the field is correct', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: returnDataFunction
        });

        // Run
        await renderComponent();
        const sendButtons = screen.getAllByRole('button')[1];
        const input = screen.getAllByRole('combobox')[0];
        await userEvent.clear(input);
        await userEvent.tab();
        await userEvent.type(input, 'technology');
        sendButtons.click();

        // Expect
        expect(returnDataFunction.mock.calls.length).toEqual(1);
    });

    test('Should alert when the api call throws an error', async () => {
        // Setup
        const alertMock = jest.spyOn(window,'alert').mockImplementation();
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockImplementation(() => {
                throw new Error();
            })
        });

        // Run
        await renderComponent();
        const sendButtons = screen.getAllByRole('button')[1];
        const input = screen.getAllByRole('combobox')[0];
        await userEvent.clear(input);
        await userEvent.tab();
        await userEvent.type(input, 'technology');
        sendButtons.click();

        // Expect
        expect(alertMock).toHaveBeenCalledTimes(1);
    });
});
