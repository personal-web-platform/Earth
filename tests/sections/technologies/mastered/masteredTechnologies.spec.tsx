/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { act } from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

import { dictionary } from '../../../../src/dictionary';
import * as useApiCall from '../../../../src/hooks/useApiCall';
import MasteredTechnologies from '../../../../src/sections/technologies/mastered/MasteredTechnologies';
import cache from '../../../../src/utils/cache';

describe('Mastered technologies section test suite', () => {
    const mockStore = configureMockStore();
    const renderComponent = async (isLoggedIn: boolean) => {
        return render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: isLoggedIn,
                    role: [dictionary.roles.user, dictionary.roles.admin]
                }
            })}>
                <MasteredTechnologies/>
                <div id="portal-root"></div>
            </Provider>
        );
    };

    const mockData = [
        {
            'name': 'technologyName',
            'icon': 'https://link.com',
            'link': 'https://link.com',
            'category': 'frontend',
            'status': 'mastered'
        }
    ];

    beforeEach(() => {
        jest.useFakeTimers();
    });

    afterEach(() => {
        jest.useRealTimers();
        jest.restoreAllMocks();
    });

    test('Should render the section title', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(false);
        });
        const title = screen.getByText(dictionary.sections.technologies.masteredTechnologies);

        // Expect
        expect(title).toBeDefined();
        expect(title).toBeInTheDocument();
    });

    test('Should show a loader while fetching projects', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: true,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        const { container } = await renderComponent(false);
        const loaders = container.getElementsByClassName('loader');

        // Expect
        expect(loaders.length).toEqual(1);
    });

    test('Should render an error if no data has been fetched', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: true,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await renderComponent(false);
        act(() => {
            jest.runAllTimers();
        });
        const errorMessage = screen.getByText('An error occured while fetching the data');

        // Expect
        expect(errorMessage).toBeDefined();
        expect(errorMessage).toBeInTheDocument();
    });

    test('Should render a table with mastered technologies', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(false);
            jest.runAllTimers();
        });
        const table = screen.getByRole('table');
        const technologyName = screen.getByText('technologyName');
        const technologyLink = screen.getByRole('link');

        // Expect
        expect(table).toBeInTheDocument();
        expect(table).toBeDefined();
        expect(technologyName).toBeInTheDocument();
        expect(technologyName).toBeDefined();
        expect(technologyLink).toBeInTheDocument();
        expect(technologyLink).toBeDefined();
    });

    test('Should use the cache if the data has been cached', async () => {
        // Setup
        const mockedFunction = jest.fn().mockResolvedValue(mockData);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: mockedFunction
        });
        jest.spyOn(cache, 'get').mockReturnValue(mockData);

        // Run
        await act(async () => {
            await renderComponent(false);
            jest.runAllTimers();
        });
        const table = screen.getByRole('table');
        const technologyName = screen.getByText('technologyName');
        const technologyLink = screen.getByRole('link');

        // Expect
        expect(table).toBeInTheDocument();
        expect(table).toBeDefined();
        expect(technologyName).toBeInTheDocument();
        expect(technologyName).toBeDefined();
        expect(technologyLink).toBeInTheDocument();
        expect(technologyLink).toBeDefined();
        expect(mockedFunction.mock.calls.length).toEqual(0);
    });

    test('Should display the admin buttons if user is logged in as admin', async () => {
        // Setup
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(true);
            jest.runAllTimers();
        });
        const buttons = screen.getAllByRole('button');

        // Expect
        expect(buttons.length).toEqual(3);
    });

    test('Should refresh the table when clicking on the refresh button', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const returnDataFunction = jest.fn().mockResolvedValue(mockData);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: returnDataFunction
        });

        // Run
        await act(async () => {
            await renderComponent(true);
            jest.runAllTimers();
        });

        await act(async () => {
            const button = screen.getByText(dictionary.sections.technologies.adminButton.masteredTechnologies.refresh);
            await button.click();
        });

        // Expect
        expect(returnDataFunction).toHaveBeenCalledTimes(2);
    });

    test('Should open the add mastered technology popup when clicking on its button', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(true);
            jest.runAllTimers();
        });
        await act(async () => {
            const button = screen.getByText(dictionary.sections.technologies.adminButton.masteredTechnologies.add);
            button.click();
        });
        const inputs = screen.getAllByRole('textbox');

        // Expect
        expect(inputs.length).toEqual(4);
    });

    test('Should open the delete mastered technology popup when clicking on its button', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockResolvedValue(mockData)
        });

        // Run
        await act(async () => {
            await renderComponent(true);
            jest.runAllTimers();
        });
        await act(async () => {
            const button = screen.getByText(dictionary.sections.technologies.adminButton.masteredTechnologies.delete);
            button.click();
        });
        const inputs = screen.getAllByRole('combobox');

        // Expect
        expect(inputs.length).toEqual(1);
    });
});