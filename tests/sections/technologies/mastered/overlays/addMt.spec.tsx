/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

import * as useApiCall from '../../../../../src/hooks/useApiCall';
import * as useInputField from '../../../../../src/hooks/useInputField';
import AddMT from '../../../../../src/sections/technologies/mastered/overlays/AddMT';

describe('Add learning technology overlay test suite', () => {
    const modalRoot = global.document.createElement('div');
    modalRoot.setAttribute('id', 'portal-root');
    const body = global.document.querySelector('body');
    body ? body.appendChild(modalRoot) : null;

    const handleCloseMock = jest.fn();
    const handleFetchMasteredTechnologiesMock = jest.fn();

    const returnDataFunction = jest.fn().mockResolvedValue(null);

    const mockStore = configureMockStore();
    const renderComponent = async () => {
        return render(
            <Provider store={mockStore({ user: { token: 'token' } })}>
                <AddMT handleClose={handleCloseMock} open={true} handleFetchMasteredTechnologies={handleFetchMasteredTechnologiesMock}/>
            </Provider>
        );
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('Should render the portal', async () => {
        // Run
        await renderComponent();
        const inputs = screen.getAllByRole('textbox');
        const closeButton = screen.getByRole('img');

        // Expect
        expect(inputs.length).toEqual(4);
        expect(closeButton).toBeInTheDocument();
        expect(closeButton).toBeDefined();
    });

    test('Should send an api request if the fields are correct', async () => {
        // Setup
        jest.spyOn(useInputField, 'default').mockReturnValue({
            userValue: {
                value: 'input',
                edited: true,
                valid: true
            },
            handleValueReset: () => {},
            handleValueChange: () => {},
            buildInputFieldCssClasses: () => ''
        });
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: returnDataFunction
        });

        // Run
        await renderComponent();
        const sendButtons = screen.getAllByRole('button')[1];
        sendButtons.click();

        // Expect
        expect(returnDataFunction.mock.calls.length).toEqual(1);
    });

    test('Should alert when the api call throws an error', async () => {
        // Setup
        const alertMock = jest.spyOn(window,'alert').mockImplementation();
        jest.spyOn(useInputField, 'default').mockReturnValue({
            userValue: {
                value: 'input',
                edited: true,
                valid: true
            },
            handleValueReset: () => {},
            handleValueChange: () => {},
            buildInputFieldCssClasses: () => ''
        });
        jest.spyOn(useApiCall, 'useApiCall').mockReturnValue({
            isLoading: false,
            error: '',
            sendRequest: jest.fn().mockImplementation(() => {
                throw new Error();
            })
        });

        // Run
        await renderComponent();
        const sendButtons = screen.getAllByRole('button')[1];
        sendButtons.click();

        // Expect
        expect(alertMock).toHaveBeenCalledTimes(1);
    });
});