/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';

import '@testing-library/jest-dom';
import Footer from '@sections/footer/Footer';
import {render, screen} from '@testing-library/react';

describe('Footer section test suite', () => {
    let alertMock: jest.SpyInstance<void, [message?: any]>;

    beforeEach(() => {
        alertMock = jest.spyOn(window, 'alert').mockImplementation();
    });

    test('Should render three footer icons', () => {
        // Run
        render(<Footer/>);
        const images = screen.getAllByRole('presentation');
        const links = screen.getAllByRole('link');

        // Expect
        expect(images.length).toEqual(3);
        expect(links.length).toEqual(3);
    });

    test('Should render the copyright', () => {
        // Run
        render(<Footer/>);
        const expectedCopyrightText = '© 2020 - ' + new Date().getFullYear() + ' francoisdevilez.eu';
        const actualCopyright = screen.getByText(expectedCopyrightText);

        // Expect
        expect(actualCopyright).toBeDefined();
        expect(actualCopyright).toHaveTextContent(expectedCopyrightText);
    });
});