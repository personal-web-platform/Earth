/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { act } from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { dictionary } from '@dictionary';
import * as useApiCall from '@hooks/useApiCall';
import Dashboard from '@sections/dashboard/Dashboard';
import { render, screen } from '@testing-library/react';
import cache from '@utils/cache';

describe('Dashboard section test suite', () => {

    const mockStatistics = {
        'success': true,
        'data': {
            'success': true,
            'data': {
                'totalNumberOfLogs': 29951,
                'totalNumberOfProjects': 9,
                'totalNumberOfDays': 47,
                'numberOfLogsPerProject': [
                    {
                        'projectName': 'Neptune',
                        'total': 4375
                    },
                    {
                        'projectName': 'Moon',
                        'total': 1729
                    },
                    {
                        'projectName': 'Mercury',
                        'total': 8489
                    },
                    {
                        'projectName': 'Sun',
                        'total': 15355
                    },
                    {
                        'projectName': 'Star',
                        'total': 0
                    },
                    {
                        'projectName': 'Earth',
                        'total': 0
                    },
                    {
                        'projectName': 'Database',
                        'total': 0
                    },
                    {
                        'projectName': 'Database2',
                        'total': 0
                    },
                    {
                        'projectName': 'Pwp-testware',
                        'total': 0
                    }
                ],
                'numberOfLogsPerDay': [
                    {
                        'date': '2023-02-22T00:00:00.000Z',
                        'total': 946
                    },
                    {
                        'date': '2023-02-23T00:00:00.000Z',
                        'total': 1566
                    },
                    {
                        'date': '2023-02-24T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-02-25T00:00:00.000Z',
                        'total': 384
                    },
                    {
                        'date': '2023-02-26T00:00:00.000Z',
                        'total': 290
                    },
                    {
                        'date': '2023-02-27T00:00:00.000Z',
                        'total': 114
                    },
                    {
                        'date': '2023-02-28T00:00:00.000Z',
                        'total': 112
                    },
                    {
                        'date': '2023-03-01T00:00:00.000Z',
                        'total': 276
                    },
                    {
                        'date': '2023-03-02T00:00:00.000Z',
                        'total': 390
                    },
                    {
                        'date': '2023-03-03T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-03-04T00:00:00.000Z',
                        'total': 390
                    },
                    {
                        'date': '2023-03-05T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-06T00:00:00.000Z',
                        'total': 1006
                    },
                    {
                        'date': '2023-03-07T00:00:00.000Z',
                        'total': 1747
                    },
                    {
                        'date': '2023-03-08T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-09T00:00:00.000Z',
                        'total': 470
                    },
                    {
                        'date': '2023-03-10T00:00:00.000Z',
                        'total': 362
                    },
                    {
                        'date': '2023-03-11T00:00:00.000Z',
                        'total': 380
                    },
                    {
                        'date': '2023-03-12T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-13T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-14T00:00:00.000Z',
                        'total': 876
                    },
                    {
                        'date': '2023-03-15T00:00:00.000Z',
                        'total': 500
                    },
                    {
                        'date': '2023-03-16T00:00:00.000Z',
                        'total': 372
                    },
                    {
                        'date': '2023-03-17T00:00:00.000Z',
                        'total': 384
                    },
                    {
                        'date': '2023-03-18T00:00:00.000Z',
                        'total': 264
                    },
                    {
                        'date': '2023-03-19T00:00:00.000Z',
                        'total': 238
                    },
                    {
                        'date': '2023-03-20T00:00:00.000Z',
                        'total': 342
                    },
                    {
                        'date': '2023-03-21T00:00:00.000Z',
                        'total': 2473
                    },
                    {
                        'date': '2023-03-22T00:00:00.000Z',
                        'total': 384
                    },
                    {
                        'date': '2023-03-23T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-03-24T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-03-25T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-03-26T00:00:00.000Z',
                        'total': 390
                    },
                    {
                        'date': '2023-03-27T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-28T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-03-29T00:00:00.000Z',
                        'total': 396
                    },
                    {
                        'date': '2023-03-30T00:00:00.000Z',
                        'total': 539
                    },
                    {
                        'date': '2023-03-31T00:00:00.000Z',
                        'total': 3073
                    },
                    {
                        'date': '2023-04-01T00:00:00.000Z',
                        'total': 380
                    },
                    {
                        'date': '2023-04-02T00:00:00.000Z',
                        'total': 3035
                    },
                    {
                        'date': '2023-04-03T00:00:00.000Z',
                        'total': 772
                    },
                    {
                        'date': '2023-04-04T00:00:00.000Z',
                        'total': 474
                    },
                    {
                        'date': '2023-04-05T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-04-06T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-04-07T00:00:00.000Z',
                        'total': 402
                    },
                    {
                        'date': '2023-04-08T00:00:00.000Z',
                        'total': 424
                    },
                    {
                        'date': '2023-04-09T00:00:00.000Z',
                        'total': 601
                    }
                ],
                'averageNumberOfLogsPerProject': [
                    {
                        'projectName': 'Neptune',
                        'average': 292
                    },
                    {
                        'projectName': 'Moon',
                        'average': 133
                    },
                    {
                        'projectName': 'Mercury',
                        'average': 181
                    },
                    {
                        'projectName': 'Sun',
                        'average': 327
                    },
                    {
                        'projectName': 'Star',
                        'average': 0
                    },
                    {
                        'projectName': 'Earth',
                        'average': 0
                    },
                    {
                        'projectName': 'Database',
                        'average': 0
                    },
                    {
                        'projectName': 'Database2',
                        'average': 0
                    },
                    {
                        'projectName': 'Pwp-testware',
                        'average': 0
                    }
                ]
            },
            'error': []
        },
        'error': []
    };

    const mockLogs = {
        'success': true,
        'data': {
            'totalNumberOfLogs': 20378,
            'logs': [
                {
                    'id': 20378,
                    'project': 'Sun',
                    'level': 'INFO',
                    'message': 'New log received from: Moon',
                    'endpoint': '-',
                    'requestMethod': '-',
                    'requestIp': '-',
                    'requestHostname': '-',
                    'createdAt': 'Thu, 16 Mar 2023 12:07:51 GMT'
                },
                {
                    'id': 20377,
                    'project': 'Sun',
                    'level': 'INFO',
                    'message': 'New log received from: Moon',
                    'endpoint': '-',
                    'requestMethod': '-',
                    'requestIp': '-',
                    'requestHostname': '-',
                    'createdAt': 'Thu, 16 Mar 2023 12:07:51 GMT'
                }
            ]
        },
        'error': []
    };

    const mockStore = configureMockStore();
    const renderComponent = async () => {
        return render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: true,
                    role: [dictionary.roles.admin, dictionary.roles.user],
                    token: 'randomToken'
                }
            })}>
                <Dashboard/>
            </Provider>
        );
    };

    global.ResizeObserver = require('resize-observer-polyfill');
    window.alert = jest.fn();

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('Should render the graphs title', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const sendRequestMock = jest.fn().mockResolvedValue(mockStatistics);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: false,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        const logRepartitionGraphTitle = screen.getByText(dictionary.sections.dashboard.graphTitles.logRepartitionPerProject);
        const dailyAverageLogsPerProjectGraphTitle = screen.getByText(dictionary.sections.dashboard.graphTitles.dailyAverageLogPerProject);
        const dailyAverageOfLogsGraphTitle = screen.getByText(dictionary.sections.dashboard.graphTitles.dailyAverageOfLogs);
        expect(logRepartitionGraphTitle).toBeInTheDocument();
        expect(logRepartitionGraphTitle).toBeDefined();
        expect(dailyAverageLogsPerProjectGraphTitle).toBeInTheDocument();
        expect(dailyAverageLogsPerProjectGraphTitle).toBeDefined();
        expect(dailyAverageOfLogsGraphTitle).toBeInTheDocument();
        expect(dailyAverageOfLogsGraphTitle).toBeDefined();
    });

    test('Should show loaders while fetching statistics and logs', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const sendRequestMock = jest.fn().mockResolvedValue([]);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: true,
                error: '',
                sendRequest: sendRequestMock
            });
        let loaders: HTMLCollectionOf<Element>;

        // Run
        await act(async () => {
            const { container } = await renderComponent();
            loaders = container.getElementsByClassName('loader');
        });

        // Expect
        // @ts-ignore
        expect(loaders.length).toEqual(4);
    });

    test('Should use the cache when fetching statistics', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(mockStatistics.data.data);
        const sendRequestMock = jest.fn().mockResolvedValue(mockStatistics.data.data);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: true,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        // Expect
        // Expected 1 here because of the request fetching logs
        expect(sendRequestMock).toHaveBeenCalledTimes(1);
    });

    test('Should call the statistics endpoint', async () => {
        // Setup
        const putSpy = jest.spyOn(cache, 'put');
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const sendRequestMock = jest.fn().mockResolvedValue(mockStatistics);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: false,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        // Expect
        expect(sendRequestMock).toHaveBeenCalledTimes(2);
        // expect(sendRequestMock.mock.calls[1][0].url).toEqual('/logs/statistics');
        expect(putSpy).toHaveBeenCalledTimes(1);
    });

    test('Should call the log endpoint', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        jest.spyOn(window, 'alert').mockImplementation();
        const sendRequestMock = jest.fn().mockResolvedValue(mockLogs);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: false,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        // Expect
        expect(sendRequestMock).toHaveBeenCalledTimes(2);
        expect(sendRequestMock.mock.calls[0][0].url).toEqual('/logs?pageNumber=0&pageSize=50');
    });

    test('Should fetch new logs when clicking on the next page button', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const sendRequestMock = jest.fn().mockResolvedValue(mockLogs.data);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: false,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        await act(async () => {
            const buttons = screen.getAllByRole('button');
            const nextPageButton = buttons[1];
            await nextPageButton.click();
        });

        // Expect
        // fetch logs, statistics, logs
        expect(sendRequestMock).toHaveBeenCalledTimes(3);
    });

    test('Should fetch new logs when clicking on the previous page button', async () => {
        // Setup
        jest.spyOn(cache, 'get').mockReturnValue(null);
        const sendRequestMock = jest.fn().mockResolvedValue(mockLogs.data);
        jest.spyOn(useApiCall, 'useApiCall')
            .mockReturnValue({
                isLoading: false,
                error: '',
                sendRequest: sendRequestMock
            });

        // Run
        await act(async () => {
            await renderComponent();
        });

        const buttons = screen.getAllByRole('button');
        const nextPageButton = buttons[1];
        const previousPageButton = buttons[0];

        await act(async () => {
            await nextPageButton.click();
        });
        await act(async () => {
            await previousPageButton.click();
        });

        // Expect
        // fetch logs, statistics, logs, logs
        expect(sendRequestMock).toHaveBeenCalledTimes(4);
    });
});