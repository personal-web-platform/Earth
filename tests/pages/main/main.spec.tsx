/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {act} from 'react';
import axios from 'axios';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import {dictionary} from '@dictionary';
import Main from '@pages/main/Main';
import {render, screen} from '@testing-library/react';

import '../../matchMedia';

jest.mock('axios');
const mAxios = axios as jest.MockedFunction<typeof axios>;

describe('Main page test suite', () => {
    const intersectionObserverMock = () => ({
        observe: jest.fn(),
        disconnect: jest.fn()
    });
    window.IntersectionObserver = jest.fn().mockImplementation(intersectionObserverMock);
    window.alert = jest.fn();
    const mockStore = configureMockStore();

    const renderComponent = (isLoggedIn: boolean) => {
        const {container, debug} = render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: isLoggedIn,
                    role: [dictionary.roles.admin, dictionary.roles.user],
                    token: 'randomToken'
                }
            })}>
                <BrowserRouter>
                    <Routes>
                        <Route path={''} element={<Main/>}/>
                    </Routes>
                </BrowserRouter>
            </Provider>
        );
        return {container, debug};
    };

    mAxios.mockResolvedValue({
        data: JSON.stringify([])
    });

    test('Should not display the dashboard if user is not logged in', async () => {
        // Run
        await act(() => {
            renderComponent(false);
        });
        const logRepartitionGraphTitle = screen.queryByText(dictionary.sections.dashboard.graphTitles.logRepartitionPerProject);

        // Expect
        expect(logRepartitionGraphTitle).toBeNull();
    });

    test('Should display the dashboard if user is logged in', async () => {
        // Setup
        let logTable;

        // Run
        await act(() => {
            const {container} = renderComponent(true);
            logTable = container.getElementsByClassName('logTableContainer');
        });

        // Expect
        expect(logTable).toBeDefined();
    });
});