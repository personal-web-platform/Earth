/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, { act } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';

import '@testing-library/jest-dom';
import { dictionary } from '@dictionary';
import Login from '@pages/login/Login';
import * as userSlice from '@store/slices/userSlice';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const navigateSpy = jest.fn();
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => navigateSpy
}));

describe('Login page test suite', () => {
    const mockStore = configureMockStore();
    const renderComponent = (isLoggedIn: boolean) => {
        const { container, debug } = render(
            <Provider store={mockStore({
                user: {
                    isLoggedIn: isLoggedIn,
                    role: [dictionary.roles.admin, dictionary.roles.user],
                    token: 'randomToken'
                }
            })}>
                <BrowserRouter>
                    <Routes>
                        <Route path={'/'} element={<Login/>}/>
                    </Routes>
                </BrowserRouter>
            </Provider>
        );
        return { container, debug };
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('Should return to "/" page if user is logged in', () => {
        // Run
        renderComponent(true);

        // Expect
        expect(navigateSpy).toHaveBeenCalledTimes(1);
    });

    test('Should contain two input fields and one button', () => {
        // Run
        const { container } = renderComponent(false);
        const inputFields = container.getElementsByClassName('input');
        const button = screen.getByRole('button');

        // Expect
        expect(inputFields.length).toEqual(2);
        expect(button).toBeInTheDocument();
        expect(button).toBeDefined();
    });

    test('Should dispatch login event when pressing the login button', async () => {
        // Setup
        const handleLoginSpy = jest.spyOn(userSlice, 'handleLoginRequest').mockReturnValue({
            type: '',
            success: true
        } as never);

        // Run
        const { container } = renderComponent(false);
        const inputFields = container.getElementsByClassName('input');
        const button = screen.getByRole('button');
        await act(async () => {
            await userEvent.type(inputFields[0], 'test@mail.com');
            await userEvent.type(inputFields[1], 'password');
            await button.click();
        });

        // Expect
        expect(handleLoginSpy).toHaveBeenCalledTimes(1);
    });

    test('Should not dispatch login event when pressing the login button if inputs are invalid', async () => {
        // Setup
        const handleLoginSpy = jest.spyOn(userSlice, 'handleLoginRequest').mockReturnValue({
            type: '',
            success: true
        } as never);

        // Run
        const { container } = renderComponent(false);
        const inputFields = container.getElementsByClassName('input');
        const button = screen.getByRole('button');
        await act(async () => {
            await userEvent.type(inputFields[0], 'test');
            await userEvent.type(inputFields[1], 'password');
            await button.click();
        });

        // Expect
        expect(handleLoginSpy).toHaveBeenCalledTimes(0);
    });
});