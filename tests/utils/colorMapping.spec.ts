/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import '@testing-library/jest-dom';
import { getColor } from '@utils/colorMapping';

describe('colorMapping test suite', () => {
    const projects = [
        'neptune',
        'moon',
        'mercury',
        'sun',
        'star',
        'earth',
        'database',
        'database2',
        'pwp-testware',
        'undefined'
    ];
    const colors = [
        '#8aa3ff',
        '#b5b5b5',
        '#806f63',
        '#fdff8a',
        '#fff6f0',
        '#97ff8a',
        'pink',
        'orange',
        'lightblue',
        'white'
    ];

    test('Should return right color for Neptune', () => {
        // Run
        const color = getColor(projects[0]);

        // Expect
        expect(color).toEqual(colors[0]);
    });

    test('Should return right color for Moon', () => {
        // Run
        const color = getColor(projects[1]);

        // Expect
        expect(color).toEqual(colors[1]);
    });

    test('Should return right color for Mercury', () => {
        // Run
        const color = getColor(projects[2]);

        // Expect
        expect(color).toEqual(colors[2]);
    });

    test('Should return right color for Sun', () => {
        // Run
        const color = getColor(projects[3]);

        // Expect
        expect(color).toEqual(colors[3]);
    });

    test('Should return right color for Star', () => {
        // Run
        const color = getColor(projects[4]);

        // Expect
        expect(color).toEqual(colors[4]);
    });

    test('Should return right color for Earth', () => {
        // Run
        const color = getColor(projects[5]);

        // Expect
        expect(color).toEqual(colors[5]);
    });

    test('Should return right color for Database', () => {
        // Run
        const color = getColor(projects[6]);

        // Expect
        expect(color).toEqual(colors[6]);
    });

    test('Should return right color for Database2', () => {
        // Run
        const color = getColor(projects[7]);

        // Expect
        expect(color).toEqual(colors[7]);
    });

    test('Should return right color for Pwp-testware', () => {
        // Run
        const color = getColor(projects[8]);

        // Expect
        expect(color).toEqual(colors[8]);
    });

    test('Should return right color for default project', () => {
        // Run
        const color = getColor(projects[9]);

        // Expect
        expect(color).toEqual(colors[9]);
    });

    test('Should make sure the project name is lower case', () => {
        // Run
        const projectName = 'NEPTUNE';
        const spy = jest.spyOn(String.prototype, 'toLowerCase');
        const color = getColor(projectName);

        // Expect
        expect(spy.mock.calls.length).toEqual(1);
    });
});