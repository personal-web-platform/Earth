/*
 *
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import axios from 'axios';

import '@testing-library/jest-dom';
import { getApiUrl, getAuthApiUrl } from '@environment';
import { useApiCallInterface } from '@interfaces';
import { sendRequest } from '@utils/sendRequest';

jest.mock('axios');
const mAxios = axios as jest.MockedFunction<typeof axios>;

describe('sendRequest helper test suite', () => {
    const mockRequestConfig = {
        method: 'GET',
        url: '/list',
        headers: {},
        dataTransformer: jest.fn(),
        authRequest: false,
        body: {}
    } as useApiCallInterface;
    const processedMockRequestConfig = {
        method: 'GET',
        url: getApiUrl() + '/list',
        headers: {},
        data: {},
        responseType: 'text'
    };

    afterEach(() => {
        jest.resetAllMocks();
        jest.restoreAllMocks();
    });

    test('Should throw an error if the api replies with success false', async () => {
        // Setup
        const mockResponse = {
            data: JSON.stringify({
                success: false,
                data: [],
                error: 'new error'
            })
        };
        mAxios.mockResolvedValue(mockResponse);

        // Run
        try {
            await sendRequest(mockRequestConfig);
        } catch (e) {

            // Expect
            const result = (e as Error);
            expect(mAxios).toHaveBeenCalledTimes(1);
            expect(result.message).toEqual('new error');
        }
    });

    test('Should call axios with the given requestConfig', async () => {
        // Setup
        const mockResponse = {
            data: JSON.stringify({
                success: true,
                data: [],
                error: []
            })
        };
        mAxios.mockResolvedValue(mockResponse);

        // Run
        await sendRequest(mockRequestConfig);

        // Expect
        expect(mAxios).toHaveBeenCalledTimes(1);
        expect(mAxios).toHaveBeenCalledWith(processedMockRequestConfig);
    });

    test('Should call the dataTransformer if success', async () => {
        // Setup
        const mockResponse = {
            data: JSON.stringify({
                success: true,
                data: [],
                error: []
            })
        };
        mAxios.mockResolvedValue(mockResponse);

        // Run
        await sendRequest(mockRequestConfig);

        // Expect
        expect(mAxios).toHaveBeenCalledTimes(1);
        expect(mAxios).toHaveBeenCalledWith(processedMockRequestConfig);
        expect(mockRequestConfig.dataTransformer).toHaveBeenCalledTimes(1);
    });

    test('Should return the data if success', async () => {
        // Setup
        const mockResponse = { data: JSON.stringify({ data: 'test' }) };
        mAxios.mockResolvedValue(mockResponse);

        // Run
        const response = await sendRequest(mockRequestConfig);

        // Expect
        expect(mAxios).toHaveBeenCalledTimes(1);
        expect(mAxios).toHaveBeenCalledWith(processedMockRequestConfig);
        expect(mockRequestConfig.dataTransformer).toHaveBeenCalledTimes(1);
        expect(response).toEqual({ data: 'test' });
    });

    test('Should change to auth url is auth request', async () => {
        // Setup
        const mockResponse = {
            data: JSON.stringify({
                success: true,
                data: [{ data: 'test' }],
                error: []
            })
        };
        const mockAuthRequestConfig = {
            method: 'GET',
            url: '/list',
            headers: {},
            dataTransformer: jest.fn(),
            authRequest: true,
            body: {}
        } as useApiCallInterface;
        const processedMockAuthRequestConfig = {
            method: 'GET',
            url: getAuthApiUrl() + '/list',
            headers: {},
            data: {},
            responseType: 'text'
        };
        mAxios.mockResolvedValue(mockResponse);

        // Run
        await sendRequest(mockAuthRequestConfig);

        // Expect
        expect(mAxios).toHaveBeenCalledTimes(1);
        expect(mAxios).toHaveBeenCalledWith(processedMockAuthRequestConfig);
    });
});