/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import cache from '@utils/cache';

describe('Cache test suite', () => {
    afterEach(() => {
        cache.resetCache();
    });

    describe('get()', () => {
        test('Should return the value from cache', () => {
            // Setup
            cache.put('url', {} as Response);

            // Run
            const result = cache.get('url');

            // Expect
            expect(result).toEqual({});
        });

        test('Should return undefined if the value is not in the cache', () => {
            // Run
            const result = cache.get('url');

            // Expect
            expect(result).toBeUndefined();
        });
    });

    describe('put()', () => {
        test('Should insert in the cache', () => {
            // Setup
            cache.put('url', {} as Response);

            // Run
            const result = cache.get('url');

            // Expect
            expect(result).toEqual({});
        });

        test('Should delete entries in cache older than the max age', () => {
            // Setup
            jest.spyOn(Date, 'now').mockReturnValueOnce(1000);
            cache.put('url', {} as Response);
            jest.spyOn(Date, 'now').mockReturnValueOnce(100000000000000000);
            cache.put('url2', {} as Response);

            // Run
            const cached = cache.get('url');

            // Expect
            expect(cached).toBeUndefined();
        });
    });

    describe('isCacheEmpty()', () => {
        test('Should return true if cache is empty', () => {
            // Run
            const result = cache.isCacheEmpty();

            // Expect
            expect(result).toBeTruthy();
        });
        test('Should return false if cache is not empty', () => {
            // Setup
            cache.put('url', {} as Response);

            // Run
            const result = cache.isCacheEmpty();

            // Expect
            expect(result).toBeFalsy();
        });
    });

    describe('resetCache()', () => {
        test('Should empty the cache', () => {
            // Setup
            cache.put('url', {} as Response);

            // Run
            cache.resetCache();

            // Expect
            const result = cache.get('url');
            expect(result).toBeUndefined();
        });
    });
});