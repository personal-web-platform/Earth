/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
    checkIfUserIsRole,
    convertIsoStringDateToDate,
    handleCheckIfTokenIsExpired,
    moveItemInArray,
    processAverageLogsPerDayData,
    processAverageLogsPerProjectData,
    processLogRepartitionData,
    processRawLearningTechnologies,
    processRawLogsData,
    processRawMasteredTechnologies,
    processRawProjectData,
    sortArray,
    validURL
} from '@utils/utils';

import {
    expectedAverageLogPerDay,
    expectedAverageLogPerProjectPerDay,
    expectedLogRepartitionData,
    expectedProcessedLearningTechnologies,
    expectedProcessedLogs,
    expectedProcessedMasteredTechnologies,
    expectedProjectData,
    pageSize,
    rawLearningTechnologies,
    rawLogs,
    rawMasteredTechnologies,
    rawProjectData,
    rawStatistics,
    roles,
    urls
} from '../mockdata';

describe('Utils test suite', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    describe('sortArray()', () => {
        let array: { name: string }[];
        let sortedArray: { name: string }[];

        beforeEach(() => {
            array = [{ name: 'c' }, { name: 'a' }, { name: 'b' }];
            sortedArray = [{ name: 'c' }, { name: 'b' }, { name: 'a' }];
        });

        test('Should sort array based on the given key', () => {
            // Run
            sortArray(array, 'name');

            // Expect
            expect(array).toEqual(sortedArray);
        });

        test('Should sort array based on the given key', () => {
            // Run
            sortArray(array, 'notInArray');

            // Expect
            expect(array).not.toEqual(sortedArray);
        });
    });

    describe('convertIsoStringDateToDate()', () => {
        test('Should format date as required', () => {
            // Setup
            const date = '2023-01-16T16:39:05.179Z';
            const formattedDate = '1/16/2023';

            // Run
            const result = convertIsoStringDateToDate(date);

            // Expect
            expect(result).toEqual(formattedDate);
        });
    });

    describe('validURL()', () => {
        test('Should return true if the url is correct', () => {
            // Run
            const result = validURL(urls[0]);

            // Expect
            expect(result).toBeTruthy();
        });

        test('Should return false if the url is incorrect', () => {
            // Run
            const result = validURL(urls[1]);

            // Expect
            expect(result).toBeFalsy();
        });
    });

    describe('moveItemInArray()', () => {
        test('Should move items in array from X to Y', () => {
            // Setup
            const array = ['a', 'b', 'c', 'd'];
            const expectedArray = ['b', 'c', 'd', 'a'];
            const x = 0;
            const y = 3;

            // Run
            moveItemInArray(array, x, y);

            // Expect
            expect(array).toEqual(expectedArray);
        });
    });

    describe('handleCheckIfTokenIsExpired()', () => {
        test('Should return true if token is still valid', () => {
            // Setup
            Storage.prototype.getItem = jest.fn().mockReturnValue(Date.now() + 1000);

            // Run
            const result = handleCheckIfTokenIsExpired();

            // Expect
            expect(result).toBeTruthy();
        });

        test('Should return false if no token in local storage', () => {
            // Setup
            Storage.prototype.getItem = jest.fn().mockReturnValue(undefined);

            // Run
            const result = handleCheckIfTokenIsExpired();

            // Expect
            expect(result).toBeFalsy();
        });

        test('Should return false if token is invalid', () => {
            // Setup
            Storage.prototype.getItem = jest.fn().mockReturnValue(0);

            // Run
            const result = handleCheckIfTokenIsExpired();

            // Expect
            expect(result).toBeFalsy();
        });
    });

    describe('checkIfUserIsRole()', () => {
        test('Should return true if role is in the role array', () => {
            // Run
            const result = checkIfUserIsRole(roles, roles[0]);

            // Expect
            expect(result).toBeTruthy();
        });

        test('Should return false if role is not in the role array', () => {
            // Run
            const result = checkIfUserIsRole(roles, 'random');

            // Expect
            expect(result).toBeFalsy();
        });
    });

    describe('processRawProjectData()', () => {
        test('Should process the projects in the expected format', () => {
            // Run
            const result = processRawProjectData(rawProjectData);

            // Expect
            expect(result).toEqual(expectedProjectData);
        });
    });

    describe('processRawLearningTechnologies()', () => {
        test('Should process the learning technologies and return the expected output', () => {
            // Run
            const results = processRawLearningTechnologies(rawLearningTechnologies);

            // Expect
            expect(results).toEqual(expectedProcessedLearningTechnologies);
        });
    });

    describe('processRawMasteredTechnologies()', () => {
        test('Should process the mastered technologies and return the expected output', () => {
            // Run
            const results = processRawMasteredTechnologies(rawMasteredTechnologies);

            // Expect
            expect(results).toEqual(expectedProcessedMasteredTechnologies);
        });
    });

    describe('processRawLogsData()', () => {
        test('Should process the logs and return the expected output', () => {
            // Run
            const result = processRawLogsData(rawLogs, pageSize);

            // Expect
            expect(result.processedLogs).toEqual(expectedProcessedLogs);
            expect(result.totalNumberOfLogs).toEqual(rawLogs.totalNumberOfLogs);
        });
    });

    describe('processLogRepartitionData()', () => {
        test('Should process the statistics and return the expected output', () => {
            // Run
            const result = processLogRepartitionData(rawStatistics.data.data as any);

            // Expect
            expect(result).toEqual(expectedLogRepartitionData);
        });
    });

    describe('processAverageLogsPerProjectData()', () => {
        test('Should process the statistics and return the expected output', () => {
            // Run
            const result = processAverageLogsPerProjectData(rawStatistics.data.data as any);

            // Expect
            expect(result).toEqual(expectedAverageLogPerProjectPerDay);
        });
    });

    describe('processAverageLogsPerDayData()', () => {
        test('Should process the statistics and return the expected output', () => {
            // Run
            const result = processAverageLogsPerDayData(rawStatistics.data.data as any);

            // Expect
            expect(result).toEqual(expectedAverageLogPerDay);
        });
    });
});