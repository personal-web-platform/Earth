/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act } from 'react';

import '@testing-library/jest-dom';
import { useApiCall } from '@hooks/useApiCall';
import { useApiCallInterface } from '@interfaces';
import { renderHook } from '@testing-library/react';
import * as sendRequestHelper from '@utils/sendRequest';

describe('useApiCall hook test suite', () => {
    const mockRequestConfig = {
        method: 'GET',
        url: '/list',
        headers: {},
        dataTransformer: jest.fn(),
        authRequest: false,
        body: {}
    } as useApiCallInterface;

    test('Should call the sendRequest helper', async () => {
        // Setup
        const mockResolvedData = { data: [] };
        jest.spyOn(sendRequestHelper, 'sendRequest').mockResolvedValue(mockResolvedData);

        // Run
        const { result } = renderHook(() => useApiCall());
        let response;
        await act(async () => {
            response = await result.current.sendRequest(mockRequestConfig);
        });

        // Expect
        expect(response).toEqual(mockResolvedData);
    });
});