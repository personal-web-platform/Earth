/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act } from 'react';

import '@testing-library/jest-dom';
import usePagination from '@hooks/usePagination';
import { renderHook } from '@testing-library/react';

describe('usePagination hook test suite', () => {
    const numberOfPage = 2;

    test('Should increase the page number by 1', () => {
        // Run
        const { result } = renderHook(() => usePagination());

        // Expect
        expect(result.current.currentPage).toEqual(0);
        act(() => result.current.handleNextPage(numberOfPage));
        expect(result.current.currentPage).toEqual(1);
    });

    test('Should decrease the page number by 1', () => {
        // Run
        const { result } = renderHook(() => usePagination());
        act(() => {
            result.current.handleNextPage(numberOfPage);
        });

        // Expect
        expect(result.current.currentPage).toEqual(1);
        act(() => result.current.handlePreviousPage());
        expect(result.current.currentPage).toEqual(0);
    });

    test('Should increase the page number but not above the number of page', () => {
        // Run
        const { result } = renderHook(() => usePagination());

        // Expect
        expect(result.current.currentPage).toEqual(0);
        act(() => {
            result.current.handleNextPage(numberOfPage);
            result.current.handleNextPage(numberOfPage);
        });
        expect(result.current.currentPage).toEqual(2);
        act(() => result.current.handleNextPage(numberOfPage));
        expect(result.current.currentPage).toEqual(2);
    });

    test('Should decrease the page number but not under 0', () => {
        // Run
        const { result } = renderHook(() => usePagination());
        act(() => {
            result.current.handleNextPage(numberOfPage);
        });

        // Expect
        expect(result.current.currentPage).toEqual(1);
        act(() => result.current.handlePreviousPage());
        expect(result.current.currentPage).toEqual(0);
        act(() => result.current.handlePreviousPage());
        expect(result.current.currentPage).toEqual(0);
    });
});