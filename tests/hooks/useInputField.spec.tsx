/*
 * Copyright (C) 2023 FRANCOIS DEVILEZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { act, ChangeEvent } from 'react';

import '@testing-library/jest-dom';
import useInputField from '@hooks/useInputField';
import { renderHook } from '@testing-library/react';

describe('useInputField hook test suite', () => {
    test('Should return valid true if the user input is valid', () => {
        // Setup
        const validationFunction = (value: string) => value.trim().length >= 2;

        // Run
        const { result } = renderHook(() => useInputField(validationFunction));
        const event = { target: { value: 'value' } } as ChangeEvent<HTMLInputElement>;
        act(() => result.current.handleValueChange(event));

        // Expect
        expect(result.current.userValue.valid).toBeTruthy();
        expect(result.current.userValue.edited).toBeTruthy();
        expect(result.current.userValue.value).toEqual(event.target.value);
    });

    test('Should return valid false if the user input is not valid', () => {
        // Setup
        const validationFunction = (value: string) => value.trim().length >= 10;

        // Run
        const { result } = renderHook(() => useInputField(validationFunction));
        const event = { target: { value: 'value' } } as ChangeEvent<HTMLInputElement>;
        act(() => result.current.handleValueChange(event));

        // Expect
        expect(result.current.userValue.valid).toBeFalsy();
        expect(result.current.userValue.edited).toBeTruthy();
        expect(result.current.userValue.value).toEqual(event.target.value);
    });

    test('Should build css class valid if the user input is valid', () => {
        // Setup
        const validationFunction = (value: string) => value.trim().length >= 2;

        // Run
        const { result } = renderHook(() => useInputField(validationFunction));
        const event = { target: { value: 'value' } } as ChangeEvent<HTMLInputElement>;
        act(() => result.current.handleValueChange(event));
        const cssClass = result.current.buildInputFieldCssClasses(result.current.userValue);

        // Expect
        expect(cssClass).toEqual('input validInput');
    });

    test('Should build css class invalid if the user input is not valid', () => {
        // Setup
        const validationFunction = (value: string) => value.trim().length >= 10;

        // Run
        const { result } = renderHook(() => useInputField(validationFunction));
        const event = { target: { value: 'value' } } as ChangeEvent<HTMLInputElement>;
        act(() => result.current.handleValueChange(event));
        const cssClass = result.current.buildInputFieldCssClasses(result.current.userValue);

        // Expect
        expect(cssClass).toEqual('input invalidInput');
    });

    test('Should reset the user value, set valid to false and edited to false', () => {
        // Setup
        const validationFunction = (value: string) => value.trim().length >= 10;

        // Run
        const { result } = renderHook(() => useInputField(validationFunction));
        act(() => result.current.handleValueReset());

        // Expect
        expect(result.current.userValue.valid).toBeFalsy();
        expect(result.current.userValue.edited).toBeFalsy();
        expect(result.current.userValue.value).toEqual('');
    });
});