# Earth

## Description

Earth is the frontend part of my personal web platform. Written in ReactJS.

## Technology stack

| Name    | Version |
|---------|---------|
| ReactJS | 18.2.0  |