module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'plugin:react/recommended'
    ],
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'simple-import-sort'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    rules: {
        'react/prop-types': 1,
        semi: [
            'error',
            'always',
        ],
        indent: [
            'error',
            4,
        ],
        'linebreak-style': [
            'error',
            'unix',
        ],
        'object-curly-spacing': ['error', 'always'],
        'quotes': ['error', 'single'],
        'simple-import-sort/imports': [
            'error',
            {
                'groups': [
                    // `react` first, `next` second, then packages starting with a character
                    ['^react$', '^next', '^[a-z]'],
                    //
                    ['@'],
                    // Imports starting with `../`
                    ['^\\.\\.(?!/?$)', '^\\.\\./?$'],
                    // Style imports
                    ['^.+\\.s?css$']
                ]
            }
        ]
    },
    settings: {
        react: {
            version: 'detect'
        }
    }
};
